#include <stdio.h>
#include <stdlib.h>

int DoIt (float a, char b, char c){ printf("DoIt\n"); return a+b+c; }

void func(int ,int ); /* function Decleration*/

	float Plus     (float a, float b) { return a+b; } /* Function  definitions*/
	float Minus    (float a, float b) { return a-b; }
	float Multiply (float a, float b) { return a*b; }
	float Divide   (float a, float b) { return a/b; }

	/*How to Pass a Function Pointer as an Argument ?*/
	void Switch_With_Function_Pointer(float a, float b, float (*pt2Func)(float, float))
	{
      float result = pt2Func(a, b);  // call using function pointer

		printf("%f\n", result);
 
   }

	/*How to Return a Function Pointer ?*/
	float (*GetPtr1(const char opCode))(float, float)
   {
      if(opCode == '+')  return &Plus;
      if(opCode == '-')  return &Minus;
   }
      void (*fp)(int,int); /*decleration of function pointer*/
	  typedef float (*pt2Function)(float, float);  //array
	   // <funcArr> is an array with 10 pointers to functions which return a float
      // and take two float 
      pt2Function funcArr[10]; //array
int main()
{
	 float result;

	fp = func; /*Assigning function address to function pointer*/

	printf("Calling function using function pointer\n");

      (*fp)(1,5); /* calling function with function pointer*/

      fp(2,9);	/* calling function with function pointer*/


	  /*passing two arguments and one function pointer to a function*/
printf("passing two arguments and one function pointer to a function\n");

	Switch_With_Function_Pointer(7, 5, Minus);
	Switch_With_Function_Pointer(7, 5, Plus);
	Switch_With_Function_Pointer(7, 5, Divide);
	Switch_With_Function_Pointer(7, 5, Multiply);


	/*How to Use Arrays of Function Pointers ?*/

   // illustrate how to work with an array of function pointers
 	  	// type-definition: 'pt2Function' now can be used as type
	printf("an array with 10 pointers to functions\n");

      funcArr[0] = &Plus;
      funcArr[1] = &Minus;

	  
	   result = funcArr[0](78,9);  // call array of function pointers
		printf("%f\n", result);

	  result=funcArr[1](90,9);
	  printf("%f\n", result);

}


void func(int arg1,int arg2)
{
      printf("%d\n", (arg1+arg2));
}