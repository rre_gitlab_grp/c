#include <stdio.h>
#include <stdlib.h>
int PassPtr(int (*pt2Func)());
void * GetPtr1(char opCode );

int DoIt (int a, int b, int c)
{	printf("DoIt\n");
    return( a+b+c);
}

int DoIt2 (int a, int b, int c)
{	printf("DoIt2\n");
    return( a*b*c);
}

int DoIt3 ()
{return(0); }

//How to define array of function pointers
int (*funcArr2[10])(int, int, int);

void main(void)
{
//General variables
    int a;			//an integer declaration
    int *b;			//a pointer to an integer
    int **c;		//a pointer to a pointer to an integer
    int d[10];		//an array of 10 integers
    int *e[10];		//an array of 10 pointers to 10 integers
    int (*f)[10];	//a pointer to an array of 10 integers
    int (*g)(int);		//a pointer to a function that takes an integer argument and returns an integer
    int (*h[10])(int);	//an array of 10 pointers to functions that take an integer argument and return an int
//const key word
    const int i; int const j;		//i and j is are 'Read only ' integers
    const int *k;		//k is a pointer to a constant integer
    int *const l;		//l to be a constant pointer to an integer
    const int *const m ;	//m to be a constant pointer to a constant integer
//volatail
    volatile int status;	//Harware registers in peripherals,volatile pointers ex:ISRs, multi thread
    const volatile int reg;	//can a parameter be const and volatile //read only satatus reg
    a=PassPtr(&DoIt);
    printf("1--%d\n",a);
    a=PassPtr((int (*)()) GetPtr1('+'));
    printf("2--%d\n",a);
    a=PassPtr((int (*)()) GetPtr1('*'));
    printf("3--%d\n",a);
    funcArr2[0]=&DoIt;
    a=PassPtr(funcArr2[0]);
    printf("4--%d\n",a);
    funcArr2[1]=&DoIt3;
    a=PassPtr(funcArr2[1]);
    printf("4--%d\n",a);


}
//How to pass a function pointer
int PassPtr(int (*pt2Func)())	//argument list need not to be defined
{
    return pt2Func(12, 11, 3);	// call using function pointer

}
//How to return a function pointer
void * GetPtr1(char opCode )			// Direct solution: Function takes a char and returns a pointer to int
{									//the return type need not to be function type
    if(opCode=='+')						// <opCode>specifies which function to return
        return(&DoIt);// &Plus;
    else
        return (&DoIt2);//&Multi;
}