
#include <stdio.h>
#include <stdlib.h>

#define setbytebit(byte,bit) ((byte) | (1 << bit))
#define resetbytebit(byte,bit) ((byte) & (~(1 << bit)))

#define mul(x,y) (x*y)
#define min(x,y) ((x)<(y)?x:y)
#define STDC

 #ifndef STDC
      #error  "This compiler does not conform to the ANSI C standard."
    #endif

int main(void)
{
	int var,res,*o,*q;

	var=20;
	res=3;
	res=setbytebit(var,2);
	printf("res : %x\n",res);

	var=0xff;
	res=resetbytebit(var,2);
	printf("resetbytebit : %x\n",res);
	var=12;
	o=&var;
    q= o++;
	printf("*o++=%x,  *o=%x\n",q,*o);

	printf("%f\n",mul(-2.4,8.8));
	

}

