#include <stdio.h>
#include <stdlib.h>

#define ARSIZE 10

struct wp_char{     //structure tag
      char wp_cval;
      short wp_font;
      short wp_psize;
}ar[ARSIZE];		//structure variables

struct wp_char *wp_p;

struct wp_char infun(void); //functioning returning structure

void main(void)
{
	struct wp_char v2;	//structure variable v2
	v2=infun();

	printf("%c %d %d\n", v2.wp_cval,v2.wp_font,v2.wp_psize);

//Pointers and structures




/* get the structure, then select a member */
(*wp_p).wp_cval= 't';

printf("%c \n", (*wp_p).wp_cval);

/* the wp_cval in the structure wp_p points to */
//wp_p->wp_cval = 'x';

//printf("%c \n", wp_p->wp_cval);

}

struct wp_char infun(void)
{
      struct wp_char wp_char;		//structure tag and variable can be of same name
      wp_char.wp_cval = getchar();
      wp_char.wp_font = 2;
      wp_char.wp_psize = 10;

      return(wp_char);
}

