#include<stdio.h>
#include<malloc.h>
struct node
{
    int info;
    struct node *next;
} *front, *rear;
void enqueue(int elt);
int dequeue();
void display();
int main()
{
    int ch, elt;
    rear = NULL;
    front = NULL;
    while (1)
    {
        printf("************ Menu ***************");
        printf("\nEnter:\n1->queue\n2->dequeue\n3->Display\n4->Exit\n");
        printf("Enter your choice :: ");
        scanf("%d", &ch);
        switch (ch)
        {
            case 1:
                printf("Enter The Element Value\n");
                scanf("%d", &elt);
                enqueue(elt);
                break;
            case 2:
                elt = dequeue();
                printf("The dequeued element = %d\n", elt);
                break;
            case 3:
                display();
                break;
            default:
                printf("~~~Exit~~~");
                //getch();
                //exit(0);
                break;
        }
    }
}
void enqueue(int elt)
{
    struct node *p;
    p = (struct node*)malloc(sizeof(struct node));
    p->info = elt;
    p->next = NULL;
    if (rear == NULL || front == NULL)
        front = p;
    else
        rear->next = p;
    rear = p;
}
int dequeue()
{
    struct node *p;
    int elt;
    if (front == NULL || rear == NULL)
    {
        printf("\nUnder Flow");
        //getch();
        //exit(0);
    }
    else
    {
        p = front;
        elt = p->info;
        front = front->next;
        free(p);
    }
    return (elt);
}
void display()
{
    struct node *t;
    t = front;
    while (front == NULL || rear == NULL)
    {
        printf("\nQueue is empty");
        //getch();
        //exit(0);
    }
    while (t!= NULL)
    {
        printf("->%d", t->info);
        t = t->next;
    }
}