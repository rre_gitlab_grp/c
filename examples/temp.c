
#include <stdio.h>
#include <malloc.h>
#define SECONDS_PER_YEAR    (60 * 60 * 24 * 365)
#define MIN(A,B) ((A)<(B)? (A):(B))
#define DEBUG 0
int main ()
{
    int a[10] = {1,2,3,4,5,6,7,8,9,9};
    int b =3;
    int result;
    int *ptr = NULL;
    ptr =&a[0];
    result = MIN(*ptr++,b);
    printf("hello MIN is %d\n",result);
    printf("hello *ptr is %d \n",*ptr);
    printf("hello *ptr is %d \n",*ptr++);
    printf("hello *ptr is %d \t %d \t  %d \t %d \t %d ",*ptr++,*ptr++,*ptr++,*ptr++,*ptr++);
    printf("hello *ptr is %d \n",*ptr++);

    #if defined(DEBUG)
        // some code for debugging
        printf("debug is defined : %d",DEBUG);
    #else
    #error "DEBUG is not defined. Define DEBUG for this build."
    #endif

    char *ptr1=NULL;
    ptr1 = (char*)malloc(0);

    printf("\nSECONDS_PER_YEAR is : %u ",SECONDS_PER_YEAR);

}
